boarding_passes <- readLines("5-input.txt")

find_seat <- function(x){
  x <- unlist(strsplit(x, ""))
  rows <- 0:127
  cols <- 0:7
  for(let in x[1:7]){
    nl <- length(rows) / 2
    if(let == "F") rows <- head(rows, n = nl) else rows <- tail(rows, n = nl)
  }
  for(let in x[8:10]){
    nc <- length(cols) / 2
    if(let == "L") cols <- head(cols, n = nc) else cols <- tail(cols, n = nc)
  }
  c(rows, cols)
}

seats <- Vectorize(find_seat)(boarding_passes)

seat_ids <- seats[1, ] * 8 + seats[2, ]
max(seat_ids)

setdiff(min(seat_ids):max(seat_ids), seat_ids)
